package task.revoluta.services;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @Mock
    private InjectorService injectorService;

    @Test
    public void testCheckBalance() {
        TransferService transferService = new TransferService();
        Assert.assertTrue(transferService.checkBalance(1000,100));
        Assert.assertTrue(transferService.checkBalance(100,100));
        Assert.assertFalse(transferService.checkBalance(100,1000));
    }
}
