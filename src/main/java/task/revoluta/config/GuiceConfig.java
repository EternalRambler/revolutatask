package task.revoluta.config;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import task.revoluta.services.AccountsManager;
import task.revoluta.services.IAccountManager;
import task.revoluta.services.DataBaseService;
import task.revoluta.services.IDataBaseService;
import task.revoluta.services.ITransferService;
import task.revoluta.services.TransferService;

public class GuiceConfig extends AbstractModule {

    @Override
    protected void configure() {
        bind(ITransferService.class).to(TransferService.class);
        bind(TransferService.class).in(Scopes.SINGLETON);
        bind(IAccountManager.class).to(AccountsManager.class);
        bind(AccountsManager.class).in(Scopes.SINGLETON);
        bind(IDataBaseService.class).to(DataBaseService.class);
        bind(DataBaseService.class).in(Scopes.SINGLETON);
    }
}
