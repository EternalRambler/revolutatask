package task.revoluta;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import task.revoluta.controllers.CashTransferPostController;
import task.revoluta.services.IDataBaseService;
import task.revoluta.services.InjectorService;

import java.sql.SQLException;

public class CashTransfer {
    public static void main(String[] args) {
        InjectorService injectorService = InjectorService.getInstance();
        IDataBaseService IDataBaseService = injectorService.getInjector().getInstance(task.revoluta.services.IDataBaseService.class);
        try {
            IDataBaseService.connectToDataBase();
            IDataBaseService.createTable();
            IDataBaseService.createUser(100, 1000);
            IDataBaseService.createUser(200, 1000);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Server server = new Server(7070);
        ServletContextHandler handler = new ServletContextHandler(server, "/cashTransfer");
        handler.addServlet(CashTransferPostController.class, "/");
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
