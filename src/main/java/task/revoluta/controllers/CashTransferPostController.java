package task.revoluta.controllers;

import org.eclipse.jetty.http.HttpStatus;
import task.revoluta.services.AccountsManager;
import task.revoluta.services.InjectorService;
import task.revoluta.services.TransferService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CashTransferPostController extends HttpServlet {
    private InjectorService injectorService = InjectorService.getInstance();
    private AccountsManager accountsManager = injectorService.getInjector().getInstance(AccountsManager.class);

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        TransferService transferService = new TransferService();
        int fromClient = Integer.parseInt(req.getParameter("fromClient"));
        int fromClientValue = Integer.parseInt(req.getParameter("value"));

        int toClient = Integer.parseInt(req.getParameter("toClient"));

        if (transferService.checkBalance(accountsManager.getBalance(fromClient), fromClientValue)) {
            transferService.changeBalance(fromClient, toClient, fromClientValue);
            resp.setStatus(HttpStatus.OK_200);
            resp.getWriter().println("New value for client: " + fromClient
             + " " + accountsManager.getBalance(fromClient));
        } else {
            resp.setStatus(HttpStatus.FORBIDDEN_403);
            resp.getWriter().println("Not Enough Money for: " + fromClient
                    + " " + accountsManager.getBalance(fromClient));
        }
    }
}
