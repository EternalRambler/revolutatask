package task.revoluta.services;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IDataBaseService {
    void connectToDataBase() throws SQLException;

    void createTable() throws SQLException;

    void createUser(int id, int value) throws SQLException;

    ResultSet getUser(int id) throws SQLException;

    int updateTable(int id, int money) throws SQLException;
}
