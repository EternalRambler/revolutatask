package task.revoluta.services;

import task.revoluta.CashTransfer;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseService implements IDataBaseService {

    private Connection connection;
    private Statement statement;

    public DataBaseService () {
        System.out.println("Creating DataBaseService");
    }

    public void connectToDataBase() throws SQLException {
        try {
            String url = "jdbc:h2:mem:";
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(CashTransfer.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            statement.close();
            connection.close();
        }
    }

    public void createTable() throws SQLException {
        String sql =  "CREATE TABLE USERS " +
                "(id INTEGER not NULL, " +
                " money INTEGER, " +
                " PRIMARY KEY ( id ))";
        statement.executeUpdate(sql);
        System.out.println("Created table in given database...");
    }

    public void createUser(int id, int value) throws SQLException {
        String sql = "INSERT INTO USERS " + "VALUES (%d, %d)";
        if (checkStatementOrReconnect()) {
            System.out.println("Executing query: " + sql);
            statement.executeUpdate(String.format(sql, id, value));
        }
    }

    public ResultSet getUser(int id) throws SQLException {
        String sql = "SELECT * FROM USERS WHERE id = %d";
        if (checkStatementOrReconnect()) {
            System.out.println("Executing query: " + sql);
            return statement.executeQuery(String.format(sql, id));
        }
        return null;
    }

    public int updateTable(int id, int money) throws SQLException {
        String sql = "UPDATE USERS " + "SET money = %d WHERE id = %d";
        if (checkStatementOrReconnect()) {
            System.out.println("Executing query: " + sql);
            return statement.executeUpdate(String.format(sql, money, id));
        }
        return Integer.MIN_VALUE;
    }

    private boolean checkStatementOrReconnect() throws SQLException {
        if (statement == null) {
            if (connection == null) {
                System.out.println("Reconnect...");
                connectToDataBase();
                return true;
            } else {
                statement = connection.createStatement();
                return true;
            }
        }
        return true;
    }
}
