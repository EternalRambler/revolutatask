package task.revoluta.services;

public interface ITransferService {

     boolean checkBalance(int clientId, int valueToCheck);
     void changeBalance(int fromClientId, int toClientId, int valueToCheck);
}
