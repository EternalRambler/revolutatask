package task.revoluta.services;

public class TransferService implements ITransferService{
    private InjectorService injectorService = InjectorService.getInstance();

    public boolean checkBalance(int clientIdBalance, int valueToCheck) {
        return clientIdBalance >= (valueToCheck);
    }

    public void changeBalance(int fromClientId, int toClientId, int value) {
        AccountsManager accountsManager = injectorService.getInjector().getInstance(AccountsManager.class);
        accountsManager.increaseBalance(toClientId, value);
        accountsManager.decriesBalance(fromClientId, value);
    }
}
