package task.revoluta.services;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import task.revoluta.config.GuiceConfig;

public class InjectorService {
    private static InjectorService instance;
    private Injector injector;
    private InjectorService(){
        injector = Guice.createInjector(new GuiceConfig());
    }

    synchronized public static InjectorService getInstance() {
        if (instance == null) {
            instance = new InjectorService();
        }
        return instance;
    }

    public Injector getInjector() {
        return injector;
    }

    public <T> T getInstance(Class<T> type) {
        return injector.getInstance(type);
    }
}
