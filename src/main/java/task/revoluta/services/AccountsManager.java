package task.revoluta.services;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountsManager implements IAccountManager{

    IDataBaseService iDataBaseService
            = InjectorService.getInstance().getInstance(IDataBaseService.class);

    public int getBalance(int id) {
        try {
            ResultSet rs = iDataBaseService.getUser(id);
            while(rs.next()) {
                return rs.getInt("money");
            }
            return Integer.MIN_VALUE;
        } catch (SQLException e) {
            e.printStackTrace();
            return Integer.MIN_VALUE;
        }
    }

    public void increaseBalance(int id, int value) {
        try {
            int currentValue = getBalance(id);
            int newValue = currentValue + value;
            iDataBaseService.updateTable(id, newValue);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void decriesBalance(int id, int value) {
        try {
            int currentValue = getBalance(id);
            int newValue = currentValue - value;
            iDataBaseService.updateTable(id, newValue);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
