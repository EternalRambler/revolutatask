package task.revoluta.services;

public interface IAccountManager {
    public int getBalance(int id);
    public void increaseBalance(int id, int value);
    public void decriesBalance(int id, int value);
}
